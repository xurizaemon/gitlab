export const SCOPED_LABEL_DELIMITER = '::';

export const DropdownVariant = {
  Sidebar: 'sidebar',
  Standalone: 'standalone',
  Embedded: 'embedded',
};

export const LabelType = {
  group: 'GroupLabel',
  project: 'ProjectLabel',
};
